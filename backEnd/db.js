//db.js

// Create MongoCliente
var MongoClient = require("mongodb").MongoClient;
// Connected?
var db = null;

//DB Keys
const url =
  "mongodb+srv://dbtdl_admin:AvWBoSX1jkE3RRdX@cluster0.ajjut.mongodb.net/dbTODOLIST?retryWrites=true&w=majority";

MongoClient.connect(
  url,
  { useNewUrlParser: true, useUnifiedTopology: true },
  function (err, client) {
    if (err) throw new Error(err);

    db = client.db();
  }
);

// Close Connection
module.exports.close = function (callback) {
  if (db) {
    db.close(function (err, result) {
      console.log("Disconnected from the Data Base");
      db = null;
      callback(err);
    });
  }
};

// GET MongoDB connected DB
module.exports.get = function () {
  return db;
};
