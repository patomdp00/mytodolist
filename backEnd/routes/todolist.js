var express = require("express");
var router = express.Router();
var task_controller = require("../controllers/taskController");
const { check } = require("express-validator"); //require("express-validator/check") está deprecado

// Validation rules:
const validate_list = [
  check("number", "The number must have at least 1 character")
    .isLength({ min: 1 })
    .isNumeric(),
  check(
    "description",
    "The content must have between 3 and 120 characters"
  ).isLength({ min: 3, max: 120 }),
];

//GET all list items http://localhost:3000/todolist/
router.get("/", task_controller.task_all);

//GET list item http://localhost:3000/todolist/id42342342
router.get("/:id", task_controller.task_one);

// POST a new item, validating input
router.post("/", validate_list, task_controller.task_create);

// PUT update validating input
router.put("/:id", validate_list, task_controller.task_update_one);

// DELETE one list item
router.delete("/:id", task_controller.task_delete_one);

module.exports = router;
