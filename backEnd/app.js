var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var listRouter = require("./routes/todolist");
var cors = require("cors");

var app = express();

app.use(cors()); //Cross-communication
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/list", listRouter);

console.log("Testing console");

// Port definition
const port = 3000;
// Port listen
app.listen(port, () => {
  console.log("Open port: http://localhost:" + port);
});

//Testing POST AND GET (for Postman)
app.get("/ruta", (request, response) => {
  response.send("Hello from ruta Method: Method: " + request.method);
});
app.post("/rutaPost", (request, response) => {
  response.send("Hello from rutaPost Method: " + request.method);
});

module.exports = app;
