/* All the actions that we could require when we create a list and is related to todolist.js
i.e.: show all lists, create new task, etc.
 */

var db = require("../db");
var mongodb = require("mongodb");

// validation modules
const { validationResult } = require("express-validator");

// get MongoDB client connected to the DB
module.exports.get = function () {
  return db;
};

//show all list items //WORKS
module.exports.task_all = function (req, res, next) {
  // if null, not connected
  if (db.get() === null) {
    next(new Error("Not Connected"));
    return;
  }
  // get all the items from the db in a list
  db.get()
    .collection("list")
    .find()
    .toArray(function (err, result) {
      // if error, send error to Express
      if (err) {
        next(new Error(err));
        return;
      } else {
        // if OK send result
        res.send(result);
      }
    });
};

//get ONE item //WORKS
module.exports.task_one = function (req, res, next) {
  //  if null, not connected
  if (db.get() === null) {
    next(new Error("Not Connected"));
    return;
  }

  const filter = { _id: new mongodb.ObjectID(req.params.id) };
  //  get ONE item from the db in a list
  db.get()
    .collection("list")
    .find(filter)
    .toArray(function (err, result) {
      // if error send to Express
      if (err) {
        next(new Error(err));
        return;
      } else {
        // if OK send result
        res.send(result);
      }
    });
};

//Create task -----
module.exports.task_create = function (req, res, next) {
  //validation results ->
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  // check DB connection
  if (db.get() === null) {
    next(new Error("Conection not stablished"));
    return;
  }
  // create task
  const task = {};
  task.number = req.body.number;
  task.description = req.body.description;
  // Insert task in list collection
  db.get()
    .collection("list")
    .insertOne(task, function (err, result) {
      if (err) {
        next(new Error(err, " CREATE attempt"));
        return;
      } else {
        res.send(result);
      }
    });
};

//Update task  -- Not tested

module.exports.task_update_one = function (req, res, next) {
  //check validations
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  // check connection
  if (db.get() === null) {
    next(new Error("La conexión no está establecida"));
    return;
  }
  const filter = { _id: new mongodb.ObjectID(req.params.id) };
  const update = {
    $set: {
      number: req.body.number,
      description: req.body.description,
    },
  };
  // insert new document
  db.get()
    .collection("list")
    .updateOne(filter, update, function (err, result) {
      // if err, send to next
      if (err) {
        next(new Error(err));
        return;
      } else {
        // if OK show result
        res.send(result);
      }
    });
};

// Delete item //WORKS
module.exports.task_delete_one = function (req, res, next) {
  if (db.get() === null) {
    next(new Error("La conexión no está establecida"));
    return;
  }
  const filter = { _id: new mongodb.ObjectID(req.params.id) };
  // Delete item with filter
  db.get()
    .collection("list")
    .deleteOne(filter, function (err, result) {
      if (err) {
        next(new Error(err + " -- DELETE attempt"));
        return;
      } else {
        res.send(result);
      }
    });
};
