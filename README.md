# My To Do List

Here you can take a look at the project's [interaction video](https://www.loom.com/share/be8963c4c8244f01a88deedf2d028b89)

## BackEnd:

- Start Visual Studio Code and Open MyToDoList folder.
- Open an integrated terminal in VSC.
- Enter inside backEnd folder with the command `cd backEnd`
- In the backEnd folder run `npm install` to install the dependencies of the project
- Run `node app` to start the BackEnd in port [http://localhost:3000](http://localhost:3000)
- `cd..` to exit one level from the backEnd folder.

## FrontEnd:

- Then enter to the frontEnd folder with: `cd frontEnd`
- Inside frontEnd folder run `npm install` in order to install the required dependencies to run the project.
- `npm run start` to run the project in port [http://localhost:4200](http://localhost:4200)

## My Portfolio

I encourage you to visit my portfolio at [https://PatricioMariano.com](https://PatricioMariano.com)

Thanks!
