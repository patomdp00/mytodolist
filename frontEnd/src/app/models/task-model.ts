export class TaskModel {
  _id?: number;
  number: string;
  description: string;

  constructor(number: string, description: string) {
    this.number = number;
    this.description = description;
  }
}
