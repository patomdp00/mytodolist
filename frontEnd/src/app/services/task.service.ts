import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskModel } from '../models/task-model';

@Injectable({
  providedIn: 'root',
})
export class TaskService {
  url = 'http://localhost:3000/list/';
  constructor(private http: HttpClient) {} //httpClient inits in the constructor

  // GET all tasks from DB
  getTask(): Observable<any> {
    return this.http.get(this.url);
  }

  // DELETE task from DB
  taskRemover(id: string): Observable<any> {
    return this.http.delete(this.url + id);
  }

  // POST task to the DB
  saveTask(task: TaskModel): Observable<any> {
    return this.http.post(this.url, task);
  }

  //GET one from DB
  getOneTask(id: string): Observable<any> {
    return this.http.get(this.url + id);
  }

  // PUT task DB
  editTask(id: string, task: TaskModel): Observable<any> {
    return this.http.put(this.url + id, task);
  }
}
