import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskModel } from 'src/app/models/task-model';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-edit-create-tasks',
  templateUrl: './edit-create-tasks.component.html',
  styleUrls: ['./edit-create-tasks.component.css'],
})
export class EditCreateTasksComponent implements OnInit {
  //creates FormControl Group to contain the form properties
  taskControlGroup: FormGroup;
  title = 'Create task';
  btnName = 'Create';
  id: string;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private _taskService: TaskService,
    private aRouter: ActivatedRoute
  ) {
    this.taskControlGroup = this.fb.group({
      number: ['', Validators.required],
      description: ['', Validators.required],
    });
    this.id = this.aRouter.snapshot.paramMap.get('id')!;
  }

  ngOnInit(): void {
    this.esEditar(); //to check titles and QueryParams
  }

  addTask() {
    const TASK: TaskModel = {
      number: this.taskControlGroup.get('number')?.value,
      description: this.taskControlGroup.get('description')?.value,
    }; //to have the object in a variable

    if (this.id !== null) {
      //it means I'm editing
      this._taskService.editTask(this.id, TASK).subscribe(
        (data) => {
          console.log(data);
          alert('Task updated');
          this.router.navigate(['/']); // back to home
        },
        (error) => {
          console.log(error);
          this.taskControlGroup.reset();
        }
      );
    } else {
      //estoy agregando un producto
      this._taskService.saveTask(TASK).subscribe(
        (data) => {
          console.log(data);
          alert('Task created');
          this.router.navigate(['/']); //  back to home
        },
        (error) => {
          console.log(error);
          this.taskControlGroup.reset();
        }
      );
    }

    console.log(TASK);
  }

  // Check ID is null
  esEditar() {
    if (this.id !== null) {
      this.title = 'Edit Task';
      this.btnName = 'Update';
      console.log('test esEditar');
      this._taskService.getOneTask(this.id).subscribe((data) => {
        console.log(data);
        this.taskControlGroup.setValue({
          number: data.number,
          description: data.description,
        });
      });
    } //end if
  } //end esEditar
}
