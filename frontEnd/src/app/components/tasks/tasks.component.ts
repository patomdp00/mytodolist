import { Component, OnInit } from '@angular/core';
import { TaskModel } from 'src/app/models/task-model';
import { TaskService } from 'src/app/services/task.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css'],
})
export class TasksComponent implements OnInit {
  title = 'To do list';
  taskList: TaskModel[] = []; //Inits empty

  constructor(private _taskService: TaskService) {}

  ngOnInit(): void {
    this.getAllTasks();
  }

  //This method subscribes to the service Observable to REFRESH tasks
  getAllTasks() {
    this._taskService.getTask().subscribe(
      (data) => {
        console.log(data);
        this.taskList = data;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  //This method subscribes to the service Observable to DELETE tasks
  deleteTask(id: any) {
    this._taskService.taskRemover(id).subscribe(
      (data) => {
        console.log(data);
        alert('Task Removed');
        this.getAllTasks(); //Refresh Table
      },
      (error) => {
        console.log(error);
      }
    );
  }
}
