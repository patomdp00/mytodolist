import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditCreateTasksComponent } from './components/edit-create-tasks/edit-create-tasks.component';
import { TasksComponent } from './components/tasks/tasks.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/tasks',
    pathMatch: 'full',
  },
  {
    path: 'tasks',
    component: TasksComponent,
  },
  {
    path: 'edit-tasks/:id',
    component: EditCreateTasksComponent,
  },
  {
    path: 'create-tasks',
    component: EditCreateTasksComponent,
  },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
